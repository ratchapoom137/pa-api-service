#!/bin/bash

cd /go/src/api
go mod tidy

echo "✅ Install Go Mod Download"
go build -mod vendor
echo "✅ Start service ..."

exec realize start --run