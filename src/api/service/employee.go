package service

import (
	"api/constant"
	"api/model"
	"errors"
	"time"

	"dv.co.th/hrms/core/rest"

	"strconv"
	// "github.com/r3labs/diff"
	// "github.com/davecgh/go-spew/spew"
)

// Create employee -
func CreateEmployee(newEmp model.Employee) (emp model.Employee, err error) {

	newEmp.EndDate, _ = time.Parse(time.RFC3339, "9999-12-31T00:00:00+00:00")
	if err = newEmp.Create(); err != nil {
		return
	}

	emp = newEmp
	return
}

//Get employee and all information from ID -
func GetEmployeeFromId(id int) (employee interface{}, ok bool, result rest.APIResultStatus, total int) {
	filter := employeeSearchFilter{
		id: &id,
	}

	output, _ := searchEmployeeWithPreload(withFilter(filter))

	if output.total == 1 {
		employeeResult := output.result.([]model.Employee)[0]

		ok = true
		employee = employeeResult.GetOutput("employee")
		total = output.total
		return
	}

	result.Message = constant.ReturnConst["EmployeeNotFoundConst"].Message
	result.Code = constant.ReturnConst["EmployeeNotFoundConst"].Code

	return
}

func GetEmployeeFromIdWithInfoByInputInfoType(id int, infoTypeName string) (employee interface{}, ok bool, result rest.APIResultStatus) {
	filter := employeeSearchFilter{
		id: &id,
	}

	var infoTypePreload string
	switch infoTypeName {
	case "personnel_info":
		infoTypePreload = "EmployeePersonnelInfo"
	case "general_info":
		infoTypePreload = "EmployeeGeneralInfo"
	case "address_info":
		infoTypePreload = "EmployeeAddressInfo"
	case "contact_info":
		infoTypePreload = "EmployeeContactInfo.ContactInfoMobilePhoneInfo"
	case "emergncy_contact_info":
		infoTypePreload = "EmployeeEmergencyContactInfo"
	default:
		return
	}

	output, _ := searchEmployee(
		withPreload(infoTypePreload),
		withFilter(filter),
	)

	if output.total == 1 {
		employeeResult := output.result.([]model.Employee)[0]

		ok = true

		switch infoTypeName {
		case "personnel_info":
			employee = employeeResult.GetOutput("employee").(model.Employee).EmployeePersonnelInfo
		case "general_info":
			employee = employeeResult.GetOutput("employee").(model.Employee).EmployeeGeneralInfo
		case "address_info":
			employee = employeeResult.GetOutput("employee").(model.Employee).EmployeeAddressInfo
		case "contact_info":
			employee = employeeResult.GetOutput("employee").(model.Employee).EmployeeContactInfo
		case "emergncy_contact_info":
			employee = employeeResult.GetOutput("employee").(model.Employee).EmployeeEmergencyContactInfo
		default:
			return
		}
		return
	}

	result.Message = constant.ReturnConst["EmployeeNotFoundConst"].Message
	result.Code = constant.ReturnConst["EmployeeNotFoundConst"].Code

	return
}

//Get all employee and all information -
func GetEmployeeLists(filter map[string]interface{}) (employee interface{}, ok bool, outputResult rest.APIResultStatus, total int) {

	var empFilter employeeSearchFilter
	limit, _ := strconv.Atoi(filter["limit"].(string))
	offset, _ := strconv.Atoi(filter["offset"].(string))

	if name, ok := filter["name"].(string); ok && name != "" {
		empFilter.name = &name
	}
	if status, ok := filter["status"].(string); ok && status != "" {
		dateNow := time.Now()
		if status == "Active" {
			empFilter.active = &dateNow
		}
		if status == "Terminated" {
			empFilter.terminated = &dateNow
		}
	}
	if employee_id, ok := filter["company_employee_id"].(string); ok && employee_id != "" {
		empFilter.employee_id = &employee_id
	}
	if company, ok := filter["company_id"].(string); ok && company != "" {
		company_id, _ := strconv.Atoi(company)
		empFilter.company_id = &company_id
	}
	if user, ok := filter["user_id"].(string); ok && user != "" {
		user_id, _ := strconv.Atoi(user)
		empFilter.user_id = &user_id
	}
	if gender, ok := filter["gender"].(string); ok && gender != "" {
		empFilter.gender = &gender
	}
	if position, ok := filter["position_id"].(string); ok && position != "" {
		position_id, _ := strconv.Atoi(position)
		empFilter.position_id = &position_id
	}
	if pointOfTime, ok := filter["point_of_time"].(string); ok && pointOfTime != "" {
		empFilter.point_of_time = &pointOfTime
	}
	if citizen_id, ok := filter["citizen_id"].(string); ok && citizen_id != "" {
		empFilter.citizen_id = &citizen_id
	}
	if company_email, ok := filter["company_email"].(string); ok && company_email != "" {
		empFilter.company_email = &company_email
	}
	if work_type, ok := filter["work_type_id"].(string); ok && work_type != "" {
		work_type_id, _ := strconv.Atoi(work_type)
		empFilter.work_type_id = &work_type_id
	}

	output, _ := searchEmployeeWithPreload(withOffset(offset), withLimit(limit), withFilter(empFilter))

	if output.total >= 1 {
		employee = output.result.([]model.Employee)
		total = output.total
		ok = true
		return
	}

	outputResult.Message = constant.ReturnConst["EmployeeNotFoundConst"].Message
	outputResult.Code = constant.ReturnConst["EmployeeNotFoundConst"].Code
	return
}

func UpdateEmployee(employeeInfoInput map[string]interface{}) (output bool, err error) {

	//Update employee information
	errUpdate := updateEmployeeInformationByInfoTypeAndId(employeeInfoInput)

	if errUpdate == true {
		return output, errors.New("cannot update employee information")
	}

	return errUpdate, err
}


