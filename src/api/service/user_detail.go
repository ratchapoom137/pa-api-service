package service

import (
	"api/model"
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"dv.co.th/hrms/core"
)

type UserDetailOutPut struct {
	ID              int        `json:"id" gorm:"column:id; type:int unsigned auto_increment; primary_key; unique;"`
	UserID          int        `json:"user_id" sql:"not null" gorm:"column:user_id"`
	EmployeeID      int        `json:"employee_id" gorm:"column:employee_id"`
	CompanyID       int        `json:"company_id" gorm:"column:company_id"`
	FirstName       string     `json:"first_name" sql:"not null" gorm:"column:first_name"`
	LastName        string     `json:"last_name" sql:"not null" gorm:"column:last_name"`
	DeactivatedDate time.Time  `json:"deactivated_date" sql:"type:datetime(6)"`
	ExpiredDate		time.Time  `json:"expired_date" sql:"type:datetime(6)"`
	CreatedAt       time.Time  `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt       time.Time  `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt       *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

// CreateUserDetail --
func CreateUserDetail(userDetail model.UserDetail) (userDetailID int, err error) {

	now := time.Now()
	userDetail.ExpiredDate = now
	userDetailID, err = userDetail.Create()
	if err != nil {
		return
	}
	return
}

// GetUserDetailByID --
func GetUserDetailByID(userDetailID int) (userDetail model.UserDetail, err error) {
	app := core.GetApplication()
	db := app.DB

	if userDetailResult := db.Preload("Employee.EmployeePersonnelInfo").First(&userDetail, userDetailID); userDetailResult.Error != nil {
		err = userDetailResult.Error
		return
	}
	return
}

// GetUserDetailList --
func GetUserDetailList(filter map[string]interface{}) (userDetails []UserDetailOutPut, total int, err error) {
	app := core.GetApplication()
	db := app.DB

	subQueryEmployee := app.DB.Table(model.Employee{}.TableName()).Select("id")
	subQueryPersonnel := app.DB.Table(model.EmployeePersonnelInfo{}.TableName()).Select("employee_info_id")

	var isEmployee bool

	limit, _ := strconv.Atoi(filter["limit"].(string))
	offset, _ := strconv.Atoi(filter["offset"].(string))

	if user_id, err := strconv.Atoi(filter["user_id"].(string)); err == nil {
		db = db.Where("user_id = (?)", user_id)
	}
	if user_id_list, ok := filter["user_id_list"].([]int); ok && len(user_id_list) > 0 {
		db = db.Where("user_id IN (?)", user_id_list)
	}
	if employee_id, err := strconv.Atoi(filter["employee_id"].(string)); err == nil {
		db = db.Where("employee_id = ?", employee_id)
	}
	if company_id_list, ok := filter["company_id_list"].([]int); ok && len(company_id_list) > 0 {
		db = db.Where("company_id IN (?)", company_id_list)
	}
	if name, ok := filter["name"].(string); ok && name != "" {
		isEmployee = true
		subQueryPersonnel = subQueryPersonnel.Where("CONCAT(firstname_en, ' ', lastname_en) LIKE ?", "%"+name+"%")
		subQueryPersonnel = subQueryPersonnel.Or("CONCAT(firstname_th, ' ', lastname_th) LIKE ?", "%"+name+"%")
		subQueryEmployee = subQueryEmployee.Where("id IN (?)", subQueryPersonnel.QueryExpr())
	}
	if status, ok := filter["status"].(string); ok && status != "" {
		dateNow := time.Now()
		if status == "ACTIVE" {
			db = db.Where("deactivated_date > (?)", &dateNow)
		}
		if status == "TERMINATED" {
			db = db.Where("deactivated_date <= (?)", &dateNow)
		}
	}

	if isEmployee {
		db = db.Where("employee_id IN (?)", subQueryEmployee.QueryExpr())
		app.Logger.Info(subQueryEmployee.QueryExpr())
	}

	var userDetailsWithEmp []model.UserDetail
	if userDetailResult := db.Offset(offset).Limit(limit).Order("id desc").Preload("Employee.EmployeePersonnelInfo").Find(&userDetailsWithEmp); userDetailResult.Error != nil {
		err = errors.New("User detail not found")
		return
	}
	db.Model(model.UserDetail{}).Count(&total)

	jsonData, _ := json.Marshal(userDetailsWithEmp)
	json.Unmarshal(jsonData, &userDetails)

	for _, userDetailsWithEmpItem := range userDetailsWithEmp {
		for index, userDetailsItem := range userDetails {
			if userDetailsWithEmpItem.ID == userDetailsItem.ID && userDetailsWithEmpItem.EmployeeID != 0 {
				userDetails[index].FirstName = userDetailsWithEmpItem.Employee.EmployeePersonnelInfo.FirstNameEn
				userDetails[index].LastName = userDetailsWithEmpItem.Employee.EmployeePersonnelInfo.LastNameEn
			}
		}
	}

	return
}

// DeleteUserDetail --
func DeleteUserDetail(userDetailID int) error {
	app := core.GetApplication()
	db := app.DB
	var userDetail model.UserDetail

	if userDetailResult := db.First(&userDetail, userDetailID); userDetailResult.Error != nil {
		return userDetailResult.Error
	}
	if userDetailResult := db.Delete(model.UserDetail{ID: userDetailID}); userDetailResult.Error != nil {
		return errors.New("Cannot delete user detail")
	}
	return nil
}

// EditUserDetail --
func EditUserDetail(userDetailID int, newUserDetail model.UserDetail) error {

	oldUserDetail, err := GetUserDetailByID(userDetailID)

	if err != nil {
		return err
	}

	
	oldUserDetail.EmployeeID = ifInt(newUserDetail.EmployeeID != 0, newUserDetail.EmployeeID, oldUserDetail.EmployeeID)
	oldUserDetail.DeactivatedDate = ifDate(newUserDetail.DeactivatedDate != time.Time{}, newUserDetail.DeactivatedDate, oldUserDetail.DeactivatedDate)
	oldUserDetail.ExpiredDate = ifDate(newUserDetail.ExpiredDate != time.Time{}, newUserDetail.ExpiredDate, oldUserDetail.ExpiredDate)

	err = oldUserDetail.Edit()
	if err != nil {
		return errors.New("Cannot edit user detail")
	}

	return nil
}
