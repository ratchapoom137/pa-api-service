package service

import (
	"time"
)

func ifStr(condition bool, ifTrue string, ifFalse string) string {
	if condition {
		return ifTrue
	}
	return ifFalse
}

func ifInt(condition bool, ifTrue int, ifFalse int) int {
	if condition {
		return ifTrue
	}
	return ifFalse
}

func ifDate(condition bool, ifTrue time.Time, ifFalse time.Time) time.Time {
	if condition {
		return ifTrue
	}
	return ifFalse
}
