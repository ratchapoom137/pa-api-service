package service

import (
	"github.com/jinzhu/gorm"
	// "github.com/davecgh/go-spew/spew"
)

type searchResult struct {
	total  int
	result interface{}
}
type searchOption func(db *gorm.DB) *gorm.DB

type searchFilterInterface interface {
	Bind(*gorm.DB) *gorm.DB
}

func withFilter(filter searchFilterInterface) searchOption {
	return func(db *gorm.DB) *gorm.DB {
		db = filter.Bind(db)

		return db
	}
}

func withPreload(field string) searchOption {
	return func(db *gorm.DB) *gorm.DB {
		db = db.Preload(field)
		return db
	}
}

func withLimit(limit int) searchOption {
	return func(db *gorm.DB) *gorm.DB {
		db = db.Limit(limit)
		return db
	}
}

func withOffset(offset int) searchOption {
	return func(db *gorm.DB) *gorm.DB {
		db = db.Offset(offset)
		return db
	}
}