package service

import (
	"api/base"
	"api/model"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"time"
	// "github.com/davecgh/go-spew/spew"
	// "errors"
	// "github.com/mitchellh/mapstructure"
)

type employeeSearchFilter struct {
	id *int
	name *string
	employee_id *string
	company_id *int
	user_id *int
	gender *string
	position_id *int
	point_of_time *string
	citizen_id *string 
	active *time.Time
	terminated *time.Time
	company_email *string
	work_type_id *int
}

// Filter employee implementation -
func (esf employeeSearchFilter) Bind(db *gorm.DB) *gorm.DB {

	app := base.GetApplication()
	subQueryPersonnel :=  app.DB.Table(model.EmployeePersonnelInfo{}.TableName()).Select("employee_info_id")
	subQueryGeneral :=  app.DB.Table(model.EmployeeGeneralInfo{}.TableName()).Select("employee_info_id")
	subQueryContact :=  app.DB.Table(model.EmployeeContactInfo{}.TableName()).Select("employee_info_id")

	var isGeneral, isPersonnel, isContact  bool

	var employee model.Employee
	if esf.id != nil {
		isGeneral = true
		db = db.Where("id = (?)", esf.id).First(&employee)
	} 
	
	if esf.user_id != nil {
		db = db.Where("user_id = (?)", esf.user_id).First(&employee)
	}

	if esf.active != nil {
		db = db.Where("end_date > (?)", *esf.active)
	}

	if esf.terminated != nil {
		db = db.Where("end_date < (?)", *esf.terminated)
	}

	if esf.name != nil {
		isPersonnel = true
		subQueryPersonnel = subQueryPersonnel.Where("CONCAT(firstname_en, ' ', lastname_en) LIKE ?", "%"+*esf.name+"%")
		subQueryPersonnel = subQueryPersonnel.Or("CONCAT(firstname_th, ' ', lastname_th) LIKE ?", "%"+*esf.name+"%")
	}
	
	if esf.employee_id != nil {
		isGeneral = true
		subQueryGeneral = subQueryGeneral.Where("company_employee_id LIKE ?", "%"+*esf.employee_id+"%")
	}

	if esf.company_id != nil {
		isGeneral = true
		subQueryGeneral = subQueryGeneral.Where("company_id = ?", *esf.company_id)
	}

	if esf.gender != nil {
		isPersonnel = true
		subQueryPersonnel = subQueryPersonnel.Where("gender = ?", *esf.gender)
	}

	if esf.position_id != nil {
		isGeneral = true
		subQueryGeneral = subQueryGeneral.Where("position_id = ?", *esf.position_id)
	}

	if esf.point_of_time != nil {
		db = db.Where("start_date < ? AND ? < end_date " , esf.point_of_time, esf.point_of_time)
	}

	if esf.citizen_id != nil {
		db = db.Where("citizen_id = ?", esf.citizen_id)
	}

	if esf.company_email != nil {
		isContact = true
		subQueryContact = subQueryContact.Where("company_email = ?", *esf.company_email)
	}

	if esf.work_type_id != nil {
		isGeneral = true
		subQueryGeneral = subQueryGeneral.Where("work_type_id = ?", *esf.work_type_id)
	}

	if isPersonnel {
		db = db.Where("id IN (?)", subQueryPersonnel.QueryExpr())
		app.Logger.Info(subQueryPersonnel.QueryExpr())
	}

	if isGeneral {
		db = db.Where("id IN (?)", subQueryGeneral.QueryExpr())
		app.Logger.Info(subQueryGeneral.QueryExpr())
	}

	if isContact {
		db = db.Where("id IN (?)", subQueryContact.QueryExpr())
		app.Logger.Info(subQueryContact.QueryExpr())
	}
	
	return db
	
}

//Update employee information by infotype and id -
func updateEmployeeInformationByInfoTypeAndId(employeeInfoInput map[string]interface{}) (err bool) {
	app := base.GetApplication()
	db := app.DB
	
	if employeeInfoInput["personnel_info"] != nil {
		infoInput := employeeInfoInput["personnel_info"].(map[string]interface{})

		var inputEmployeeInfoParsed model.EmployeePersonnelInfo
		jsoInputData, _ := json.Marshal(infoInput["data"])
		json.Unmarshal(jsoInputData, &inputEmployeeInfoParsed)

		db.Model(model.EmployeePersonnelInfo{}).Where("employee_info_id = ?", infoInput["employee_id"]).Updates(inputEmployeeInfoParsed)

		return
	}
	if employeeInfoInput["address_info"] != nil {
		infoInput := employeeInfoInput["address_info"].(map[string]interface{})

		var inputEmployeeInfoParsed []model.EmployeeAddressInfo
		jsoInputData, _ := json.Marshal(infoInput["data"])
		json.Unmarshal(jsoInputData, &inputEmployeeInfoParsed)

		for _, addressInfoInput := range inputEmployeeInfoParsed {
			db.Model(model.EmployeeAddressInfo{}).Where("id = ? AND employee_info_id = ? AND address_type = ?", addressInfoInput.ID, infoInput["employee_id"], addressInfoInput.AddressType).Updates(addressInfoInput)
		}

		return
	}
	if employeeInfoInput["contact_info"] != nil {
		infoInput := employeeInfoInput["contact_info"].(map[string]interface{})

		var inputEmployeeInfoParsed model.EmployeeContactInfo
		jsoInputData, _ := json.Marshal(infoInput["data"])
		json.Unmarshal(jsoInputData, &inputEmployeeInfoParsed)

		db.Model(model.EmployeeContactInfo{}).Where("employee_info_id = ?", infoInput["employee_id"]).Updates(inputEmployeeInfoParsed)

		Employee, _, _, _ := GetEmployeeFromId(infoInput["employee_id"].(int))
		var EmployeeParsed model.Employee
		jsoEmployeeParsed, _ := json.Marshal(Employee)
		json.Unmarshal(jsoEmployeeParsed, &EmployeeParsed)

		lenContactGet := len(EmployeeParsed.EmployeeContactInfo.ContactInfoMobilePhoneInfo)-1
		lenContactInput := len(inputEmployeeInfoParsed.ContactInfoMobilePhoneInfo)-1
		for index, contactInfoInput := range inputEmployeeInfoParsed.ContactInfoMobilePhoneInfo {
			if index <= lenContactGet {
				db.Model(model.ContactInfoMobilePhoneInfo{}).Where("id = ? AND mobile_info_id = ?", contactInfoInput.ID, infoInput["employee_id"]).Updates(contactInfoInput)
			}
			if index > lenContactGet {
				db.Model(EmployeeParsed.EmployeeContactInfo).Association("ContactInfoMobilePhoneInfo").Append(contactInfoInput)
			}
		}
		if lenContactInput < lenContactGet {
			for index, contactInfoGet := range EmployeeParsed.EmployeeContactInfo.ContactInfoMobilePhoneInfo {
				if index > lenContactInput {
					db.Model(model.ContactInfoMobilePhoneInfo{}).Where("id = ? AND mobile_info_id = ?", contactInfoGet.ID, infoInput["employee_id"]).Delete(contactInfoGet)
				}
			}
		}

		return
	}
	if employeeInfoInput["emergncy_contact_info"] != nil {
		infoInput := employeeInfoInput["emergncy_contact_info"].(map[string]interface{})

		var inputEmployeeInfoParsed []model.EmployeeEmergencyContactInfo
		jsoInputData, _ := json.Marshal(infoInput["data"])
		json.Unmarshal(jsoInputData, &inputEmployeeInfoParsed)

		for _, emergencyInfoInput := range inputEmployeeInfoParsed {
			db.Model(model.EmployeeEmergencyContactInfo{}).Where("id = ? AND employee_info_id = ?", emergencyInfoInput.ID, infoInput["employee_id"]).Updates(emergencyInfoInput)
		}

		return
	}
	if employeeInfoInput["general_info"] != nil {
		infoInput := employeeInfoInput["general_info"].(map[string]interface{})
		employeeInfoInput := infoInput["data"].(map[string]interface{})

		var inputEmployeeInfoParsed model.Employee
		if employeeInfoInput["end_date"] != nil {
			jsoInputData, _ := json.Marshal(employeeInfoInput)
			json.Unmarshal(jsoInputData, &inputEmployeeInfoParsed)
			db.Model(model.Employee{}).Where("id = ?", infoInput["employee_id"]).Updates(inputEmployeeInfoParsed)
			return
		}
		if employeeInfoInput["start_date"] != nil {
			jsoInputData, _ := json.Marshal(employeeInfoInput)
			json.Unmarshal(jsoInputData, &inputEmployeeInfoParsed)
			db.Model(model.Employee{}).Where("id = ?", infoInput["employee_id"]).Updates(inputEmployeeInfoParsed)
		}

		jsoInputData, _ := json.Marshal(employeeInfoInput)
		json.Unmarshal(jsoInputData, &inputEmployeeInfoParsed.EmployeeGeneralInfo)
		db.Model(model.EmployeeGeneralInfo{}).Where("employee_info_id = ?", infoInput["employee_id"]).Updates(inputEmployeeInfoParsed.EmployeeGeneralInfo)
		
		return
	}
	err = true
	return
}

// Search employee and all information implementation -
func searchEmployeeWithPreload(options ...searchOption) (output searchResult, err error) {
	var employee []model.Employee
	var count int

	app := base.GetApplication()

	db := app.DB
	db = db.Model(&model.Employee{})

	for _, option := range options {
		db = option(db)
	}

	if outputErr := db.Preload("EmployeePersonnelInfo").Preload("EmployeeAddressInfo").Preload("EmployeeContactInfo.ContactInfoMobilePhoneInfo").Preload("EmployeeEmergencyContactInfo").Preload("EmployeeGeneralInfo").Order("id desc").Find(&employee).Error; outputErr != nil {
		app.Logger.Error(outputErr.Error())

		err = outputErr
		return
	}

	db.Limit(-1).Offset(0).Count(&count)

	output.result = employee
	output.total = count
	return
}

// Search employee and all information implementation -
func searchEmployee(options ...searchOption) (output searchResult, err error) {
	var employee []model.Employee
	var count int
	app := base.GetApplication()

	db := app.DB
	db = db.Model(&model.Employee{})

	for _, option := range options {
		db = option(db)
	}

	if outputErr := db.Find(&employee).Error; outputErr != nil {
		app.Logger.Error(outputErr.Error())

		err = outputErr
		return
	}

	db.Count(&count)

	output.result = employee
	output.total = count
	return
}
