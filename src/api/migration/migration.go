package migration

import (
	"api/base"
	"api/helpers"
	"api/model"
	"time"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

func Execute() {
	app := base.GetApplication()
	db := app.DB

	db.LogMode(true)

	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		{
			ID:       "201906232036",
			Migrate:  Migrate_201906232036,
			Rollback: Rollback_201906232036,
		},
		{
			ID:       "201908142036",
			Migrate:  Seed_201908142036,
			Rollback: Rollback_201908142036,
		},
	})

	if err := m.Migrate(); err != nil {
		app.Logger.Error("Could not migrate: ", err)
		return
	}

	app.Logger.Info("Migration done.")
}

func Migrate_201906232036(tx *gorm.DB) error {
	tx = tx.Begin()
	tx.AutoMigrate(&model.Employee{})
	tx.AutoMigrate(&model.EmployeePersonnelInfo{})
	tx.AutoMigrate(&model.EmployeeAddressInfo{})
	tx.AutoMigrate(&model.EmployeeContactInfo{})
	tx.AutoMigrate(&model.ContactInfoMobilePhoneInfo{})
	tx.AutoMigrate(&model.EmployeeGeneralInfo{})
	tx.AutoMigrate(&model.EmployeeEmergencyContactInfo{})
	tx.AutoMigrate(&model.UserDetail{})

	return tx.Commit().Error
}

func Rollback_201906232036(tx *gorm.DB) error {
	tx = tx.Begin()

	tx.DropTable(
		&model.Employee{},
		&model.EmployeePersonnelInfo{},
		&model.EmployeeAddressInfo{},
		&model.EmployeeContactInfo{},
		&model.ContactInfoMobilePhoneInfo{},
		&model.EmployeeGeneralInfo{},
		&model.EmployeeEmergencyContactInfo{},
		&model.UserDetail{},
	)
	return tx.Commit().Error
}

func Seed_201908142036(tx *gorm.DB) error {
	tx = tx.Begin()

	//Employee1
	personnelInfoModel1 := model.EmployeePersonnelInfo{
		TitleNameTh: "นางสาว",
		FirstNameTh: "ธัญชนก",
		LastNameTh:  "พรหมปาลิต",
		TitleNameEn: "Miss",
		FirstNameEn: "Thanchanok",
		LastNameEn:  "Phrompalit",
		DateOfBirth: time.Now(),
		Gender:      "Female",
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	generalInfoModel1 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP01",
		TypeOfStaff:       "Permanent",
		EmployeeLevel:     "ManagerAndAdvicer",
		WorkTypeID:        1,
		PositionID:        13,
		OrgUnitID:         16,
		CompanyBranchID:   3,
		CompanyID:         3,
		CreatedBy:         1,
		UpdatedBy:         1,
	}
	addressInfoModel1 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country:     "Thailand",
			Province:    "Chiang mai",
			Address:     "11 M.4 San Phi Suang, Mueang",
			PostalCode:  "50300",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country:     "Thailand",
			Province:    "Chiang mai",
			Address:     "11 M.4 San Phi Suang, Mueang",
			PostalCode:  "50300",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
	}
	mobilePhoneInfoModel1 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0932191666",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0932191667",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
	}
	contactInfoModel1 := model.EmployeeContactInfo{
		CompanyEmail:               "thanchanok_phr@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel1,
		CreatedBy:                  1,
		UpdatedBy:                  1,
	}
	emergencyContactInfoModel1 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name:      "Thiratee Phrompalit",
			Relative:  "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name:      "Chanaporn Phrompalit",
			Relative:  "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}

	//Employee2
	personnelInfoModel2 := model.EmployeePersonnelInfo{
		TitleNameTh: "นาย",
		FirstNameTh: "รัชภูมิ",
		LastNameTh:  "รัตนกิจ",
		TitleNameEn: "Mr.",
		FirstNameEn: "Ratchapoom",
		LastNameEn:  "Ratthanakit",
		DateOfBirth: time.Now(),
		Gender:      "Male",
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	generalInfoModel2 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP02",
		TypeOfStaff:       "Permanent",
		EmployeeLevel:     "ManagerAndAdvicer",
		WorkTypeID:        1,
		PositionID:        14,
		OrgUnitID:         16,
		CompanyBranchID:   3,
		CompanyID:         3,
		CreatedBy:         1,
		UpdatedBy:         1,
	}
	addressInfoModel2 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country:     "Thailand",
			Province:    "Bangkok",
			Address:     "124 Klongtoey",
			PostalCode:  "10110",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country:     "Thailand",
			Province:    "Bangkok",
			Address:     "124 Klongtoey",
			PostalCode:  "10110",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
	}
	mobilePhoneInfoModel2 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0952191666",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0962191667",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
	}
	contactInfoModel2 := model.EmployeeContactInfo{
		CompanyEmail:               "ratchapoom_rat@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel2,
		CreatedBy:                  1,
		UpdatedBy:                  1,
	}
	emergencyContactInfoModel2 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name:      "Thiratee Ratthanakit",
			Relative:  "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name:      "Chanaporn Ratthanakit",
			Relative:  "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}

	//Employee3
	personnelInfoModel3 := model.EmployeePersonnelInfo{
		TitleNameTh: "นางสาว",
		FirstNameTh: "ศิริพรรณ",
		LastNameTh:  "แซ่เชี๊ยะ",
		TitleNameEn: "Miss",
		FirstNameEn: "Siripan",
		LastNameEn:  "Saecheah",
		DateOfBirth: time.Now(),
		Gender:      "Female",
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	generalInfoModel3 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP03",
		TypeOfStaff:       "Probation",
		EmployeeLevel:     "Executive",
		WorkTypeID:        1,
		PositionID:        6,
		OrgUnitID:         6,
		CompanyBranchID:   4,
		CompanyID:         3,
		CreatedBy:         1,
		UpdatedBy:         1,
	}
	addressInfoModel3 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country:     "Thailand",
			Province:    "Bangkok",
			Address:     "11979/13 Sukhumvit Soi 75/1, Phra Khanong",
			PostalCode:  "10260",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country:     "Thailand",
			Province:    "Bangkok",
			Address:     "11979/13 Sukhumvit Soi 75/1, Phra Khanong",
			PostalCode:  "10260",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
	}
	mobilePhoneInfoModel3 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0932191458",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0912191672",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
	}
	contactInfoModel3 := model.EmployeeContactInfo{
		CompanyEmail:               "siripan_sae@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel3,
		CreatedBy:                  1,
		UpdatedBy:                  1,
	}
	emergencyContactInfoModel3 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name:      "Thiratee Saecheah",
			Relative:  "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name:      "Chanaporn Saecheah",
			Relative:  "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}

	//Employee4
	personnelInfoModel4 := model.EmployeePersonnelInfo{
		TitleNameTh: "นางสาว",
		FirstNameTh: "แคทรียา",
		LastNameTh:  "สุวรรณภูมิ",
		TitleNameEn: "Miss",
		FirstNameEn: "Kattareya",
		LastNameEn:  "Suwannapoom",
		DateOfBirth: time.Now(),
		Gender:      "Female",
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	generalInfoModel4 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP04",
		TypeOfStaff:       "Permanent",
		EmployeeLevel:     "MiddleManager",
		WorkTypeID:        1,
		PositionID:        7,
		OrgUnitID:         8,
		CompanyBranchID:   4,
		CompanyID:         3,
		CreatedBy:         1,
		UpdatedBy:         1,
	}
	addressInfoModel4 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country:     "Thailand",
			Province:    "Bangkok",
			Address:     "152 Chartered Square Building, Floor 30, Unit 301313 North Sathorn Road",
			PostalCode:  "10500",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country:     "Thailand",
			Province:    "Bangkok",
			Address:     "152 Chartered Square Building, Floor 30, Unit 301313 North Sathorn Road",
			PostalCode:  "10500",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
	}
	mobilePhoneInfoModel4 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0972191451",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0962191667",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
	}
	contactInfoModel4 := model.EmployeeContactInfo{
		CompanyEmail:               "kattareya_suw@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel4,
		CreatedBy:                  1,
		UpdatedBy:                  1,
	}
	emergencyContactInfoModel4 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name:      "Thiratee Suwannapoom",
			Relative:  "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name:      "Chanaporn Suwannapoom",
			Relative:  "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}

	//Employee5
	personnelInfoModel5 := model.EmployeePersonnelInfo{
		TitleNameTh: "นาย",
		FirstNameTh: "ชนบท",
		LastNameTh:  "ด้วงประสิทธิ์",
		TitleNameEn: "Mr.",
		FirstNameEn: "Chonnabot",
		LastNameEn:  "Doungparsit",
		DateOfBirth: time.Now(),
		Gender:      "Male",
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	generalInfoModel5 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP05",
		TypeOfStaff:       "Permanent",
		EmployeeLevel:     "MiddleManager",
		WorkTypeID:        1,
		PositionID:        9,
		OrgUnitID:         10,
		CompanyBranchID:   4,
		CompanyID:         3,
		CreatedBy:         1,
		UpdatedBy:         1,
	}
	addressInfoModel5 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country:     "Thailand",
			Province:    "Nakhon Ratchasima",
			Address:     "Synchrotron Light Research Institute, 111 University Avenue, Muang District",
			PostalCode:  "30000",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country:     "Thailand",
			Province:    "Nakhon Ratchasima",
			Address:     "Synchrotron Light Research Institute, 111 University Avenue, Muang District",
			PostalCode:  "30000",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
	}
	mobilePhoneInfoModel5 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0832191666",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0832191667",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
	}
	contactInfoModel5 := model.EmployeeContactInfo{
		CompanyEmail:               "chonnabot_dun@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel5,
		CreatedBy:                  1,
		UpdatedBy:                  1,
	}
	emergencyContactInfoModel5 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name:      "Thiratee Doungparsit",
			Relative:  "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name:      "Chanaporn Doungparsit",
			Relative:  "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}

	//Employee6
	personnelInfoModel6 := model.EmployeePersonnelInfo{
		TitleNameTh: "นาย",
		FirstNameTh: "ปรัชญา",
		LastNameTh:  "ฉ่ำคล้าม",
		TitleNameEn: "Mr.",
		FirstNameEn: "Phatya",
		LastNameEn:  "Chamklam",
		DateOfBirth: time.Now(),
		Gender:      "Male",
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	generalInfoModel6 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP06",
		TypeOfStaff:       "Permanent",
		EmployeeLevel:     "MiddleManager",
		WorkTypeID:        1,
		PositionID:        11,
		OrgUnitID:         11,
		CompanyBranchID:   4,
		CompanyID:         3,
		CreatedBy:         1,
		UpdatedBy:         1,
	}
	addressInfoModel6 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country:     "Thailand",
			Province:    "Nakhon Ratchasima",
			Address:     "Synchrotron Light Research Institute, 111 University Avenue, Muang District",
			PostalCode:  "30000",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country:     "Thailand",
			Province:    "Nakhon Ratchasima",
			Address:     "Synchrotron Light Research Institute, 111 University Avenue, Muang District",
			PostalCode:  "30000",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
	}
	mobilePhoneInfoModel6 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0832191666",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0832191667",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
	}
	contactInfoModel6 := model.EmployeeContactInfo{
		CompanyEmail:               "phatya_cha@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel6,
		CreatedBy:                  1,
		UpdatedBy:                  1,
	}
	emergencyContactInfoModel6 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name:      "Thiratee Chamklam",
			Relative:  "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name:      "Chanaporn Chamklam",
			Relative:  "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}

	//Employee7
	personnelInfoModel7 := model.EmployeePersonnelInfo{
		TitleNameTh: "นาย",
		FirstNameTh: "กุลภัทร",
		LastNameTh:  "ปัญญาดี",
		TitleNameEn: "Mr.",
		FirstNameEn: "Kunlapat",
		LastNameEn:  "Panyadee",
		DateOfBirth: time.Now(),
		Gender:      "Male",
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	generalInfoModel7 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP07",
		TypeOfStaff:       "Permanent",
		EmployeeLevel:     "ManagerAndAdvicer",
		WorkTypeID:        1,
		PositionID:        17,
		OrgUnitID:         18,
		CompanyBranchID:   4,
		CompanyID:         3,
		CreatedBy:         1,
		UpdatedBy:         1,
	}
	addressInfoModel7 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country:     "Thailand",
			Province:    "Bangkok",
			Address:     "88/31 Soi Timland, Ngamwongwan road, Bang Khen",
			PostalCode:  "11110",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country:     "Thailand",
			Province:    "Bangkok",
			Address:     "88/31 Soi Timland, Ngamwongwan road,Bang Khen",
			PostalCode:  "11110",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
	}
	mobilePhoneInfoModel7 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0838192666",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0862791667",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
	}
	contactInfoModel7 := model.EmployeeContactInfo{
		CompanyEmail:               "kunlapat_pun@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel7,
		CreatedBy:                  1,
		UpdatedBy:                  1,
	}
	emergencyContactInfoModel7 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name:      "Thiratee Panyadee",
			Relative:  "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name:      "Chanaporn Panyadee",
			Relative:  "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}

	//Employee8
	personnelInfoModel8 := model.EmployeePersonnelInfo{
		TitleNameTh: "นางสาว",
		FirstNameTh: "ชนาภา",
		LastNameTh:  "ศิริพัชร",
		TitleNameEn: "Miss",
		FirstNameEn: "Chanapa",
		LastNameEn:  "Siriphathara",
		DateOfBirth: time.Now(),
		Gender:      "Female",
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	generalInfoModel8 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP08",
		TypeOfStaff:       "Permanent",
		EmployeeLevel:     "ManagerAndAdvicer",
		WorkTypeID:        1,
		PositionID:        19,
		OrgUnitID:         19,
		CompanyBranchID:   4,
		CompanyID:         3,
		CreatedBy:         1,
		UpdatedBy:         1,
	}
	addressInfoModel8 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country:     "Thailand",
			Province:    "Phuket",
			Address:     "159/29 Srisoonthorn Rd, Srisoonthorn Thalang District",
			PostalCode:  "83110",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country:     "Thailand",
			Province:    "Phuket",
			Address:     "159/29 Srisoonthorn Rd, Srisoonthorn Thalang District,",
			PostalCode:  "83110",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
	}
	mobilePhoneInfoModel8 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0912191641",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0822191467",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
	}
	contactInfoModel8 := model.EmployeeContactInfo{
		CompanyEmail:               "chanapa_sir@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel8,
		CreatedBy:                  1,
		UpdatedBy:                  1,
	}
	emergencyContactInfoModel8 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name:      "Thiratee Siriphathara",
			Relative:  "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name:      "Chanaporn Siriphathara",
			Relative:  "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}

	//Employee9
	personnelInfoModel9 := model.EmployeePersonnelInfo{
		TitleNameTh: "นางสาว",
		FirstNameTh: "กฤตยาณี",
		LastNameTh:  "ธรรมราช",
		TitleNameEn: "Miss",
		FirstNameEn: "Krittayanee",
		LastNameEn:  "Thummaraj",
		DateOfBirth: time.Now(),
		Gender:      "Female",
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	generalInfoModel9 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP09",
		TypeOfStaff:       "Permanent",
		EmployeeLevel:     "ManagerAndAdvicer",
		WorkTypeID:        1,
		PositionID:        10,
		OrgUnitID:         10,
		CompanyBranchID:   4,
		CompanyID:         3,
		CreatedBy:         1,
		UpdatedBy:         1,
	}
	addressInfoModel9 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country:     "Thailand",
			Province:    "Pattaya",
			Address:     "218/2-4 M.10, Beach Rd, Pattaya City, Bang Lamung District",
			PostalCode:  "20260",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country:     "Thailand",
			Province:    "Pattaya",
			Address:     "218/2-4 M.10, Beach Rd, Pattaya City, Bang Lamung District",
			PostalCode:  "20260",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
	}
	mobilePhoneInfoModel9 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0912191641",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0822191467",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
	}
	contactInfoModel9 := model.EmployeeContactInfo{
		CompanyEmail:               "krittayanee_thu@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel9,
		CreatedBy:                  1,
		UpdatedBy:                  1,
	}
	emergencyContactInfoModel9 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name:      "Thiratee Thummaraj",
			Relative:  "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name:      "Chanaporn Thummaraj",
			Relative:  "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}

	//Employee10
	personnelInfoModel10 := model.EmployeePersonnelInfo{
		TitleNameTh: "นาย",
		FirstNameTh: "Thanakan",
		LastNameTh:  "Thanakan",
		TitleNameEn: "Mr.",
		FirstNameEn: "Thanakan",
		LastNameEn:  "Thanakan",
		DateOfBirth: time.Now(),
		Gender:      "Male",
		CreatedBy:   1,
		UpdatedBy:   1,
	}
	generalInfoModel10 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP010",
		TypeOfStaff: "Permanent",
		EmployeeLevel: "Staff",
		WorkTypeID: 1,
		PositionID: 18,
		OrgUnitID: 18,
		CompanyBranchID: 4,
		CompanyID: 3,
		CreatedBy: 1,
		UpdatedBy: 1,
	}
	addressInfoModel10 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country:     "Thailand",
			Province:    "Pattaya",
			Address:     "218/2-4 M.10, Beach Rd, Pattaya City, Bang Lamung District",
			PostalCode:  "20260",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country:     "Thailand",
			Province:    "Pattaya",
			Address:     "218/2-4 M.10, Beach Rd, Pattaya City, Bang Lamung District",
			PostalCode:  "20260",
			CreatedBy:   1,
			UpdatedBy:   1,
		},
	}
	mobilePhoneInfoModel10 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0912191641",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0822191467",
			CreatedBy:         1,
			UpdatedBy:         1,
		},
	}
	contactInfoModel10 := model.EmployeeContactInfo{
		CompanyEmail: "thanakan_tha@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel10,
		CreatedBy:                  1,
		UpdatedBy:                  1,
	}
	emergencyContactInfoModel10 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name:      "Thiratee Thummaraj",
			Relative:  "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name:      "Chanaporn Thummaraj",
			Relative:  "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}
	
	//Employee11
	personnelInfoModel11 := model.EmployeePersonnelInfo{
		TitleNameTh: "นางสาว",
		FirstNameTh: "กาสะลอง",
		LastNameTh: "ธรรมราช",
		TitleNameEn: "Miss",
		FirstNameEn: "Kasalong",
		LastNameEn: "Thummaraj",
		DateOfBirth: time.Now(),
		Gender: "Female",
		CreatedBy: 1,
		UpdatedBy: 1,
	}
	generalInfoModel11 := model.EmployeeGeneralInfo{
		CompanyEmployeeID: "EMP10",
		TypeOfStaff: "Permanent",
		EmployeeLevel: "ManagerAndAdvicer",
		WorkTypeID: 1,
		PositionID: 10,
		OrgUnitID: 10,
		CompanyBranchID: 4,
		CompanyID: 3,
		CreatedBy: 1,
		UpdatedBy: 1,
	}
	addressInfoModel11 := []model.EmployeeAddressInfo{
		model.EmployeeAddressInfo{
			AddressType: "House Registration Address",
			Country: "Thailand",
			Province: "Pattaya",
			Address: "218/2-4 M.10, Beach Rd, Pattaya City, Bang Lamung District",
			PostalCode: "20260",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeAddressInfo{
			AddressType: "Current Address",
			Country: "Thailand",
			Province: "Pattaya",
			Address: "218/2-4 M.10, Beach Rd, Pattaya City, Bang Lamung District",
			PostalCode: "20260",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}
	mobilePhoneInfoModel11 := []model.ContactInfoMobilePhoneInfo{
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0912191641",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.ContactInfoMobilePhoneInfo{
			MobilePhoneNumber: "0822191467",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	} 
	contactInfoModel11 := model.EmployeeContactInfo{
		CompanyEmail: "kasalong_thu@dv.co.th",
		ContactInfoMobilePhoneInfo: mobilePhoneInfoModel11,
		CreatedBy: 1,
		UpdatedBy: 1,
	}
	emergencyContactInfoModel11 := []model.EmployeeEmergencyContactInfo{
		model.EmployeeEmergencyContactInfo{
			Name: "Thiratee Thummaraj",
			Relative: "Father",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
		model.EmployeeEmergencyContactInfo{
			Name: "Chanaporn Thummaraj",
			Relative: "Mother",
			CreatedBy: 1,
			UpdatedBy: 1,
		},
	}

	maxTime, _ := time.Parse(time.RFC3339, "9999-12-31T00:00:00+00:00")
	tx.Create(&model.Employee{
		UserID:                       2,
		CitizenID:                    "1509970048698",
		EmployeePersonnelInfo:        &personnelInfoModel1,
		EmployeeGeneralInfo:          &generalInfoModel1,
		EmployeeAddressInfo:          addressInfoModel1,
		EmployeeContactInfo:          &contactInfoModel1,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel1,
		CreatedBy:                    1,
		UpdatedBy:                    1,
		StartDate:                    helpers.StartOfDateNow(),
		EndDate:                      maxTime,
	})
	tx.Create(&model.Employee{
		UserID:                       3,
		CitizenID:                    "1109970048698",
		EmployeePersonnelInfo:        &personnelInfoModel2,
		EmployeeGeneralInfo:          &generalInfoModel2,
		EmployeeAddressInfo:          addressInfoModel2,
		EmployeeContactInfo:          &contactInfoModel2,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel2,
		CreatedBy:                    1,
		UpdatedBy:                    1,
		StartDate:                    helpers.StartOfDateNow(),
		EndDate:                      maxTime,
	})
	tx.Create(&model.Employee{
		UserID:                       4,
		CitizenID:                    "1209970048698",
		EmployeePersonnelInfo:        &personnelInfoModel3,
		EmployeeGeneralInfo:          &generalInfoModel3,
		EmployeeAddressInfo:          addressInfoModel3,
		EmployeeContactInfo:          &contactInfoModel3,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel3,
		CreatedBy:                    1,
		UpdatedBy:                    1,
		StartDate:                    helpers.StartOfDateNow(),
		EndDate:                      maxTime,
	})
	tx.Create(&model.Employee{
		UserID:                       5,
		CitizenID:                    "1309970048698",
		EmployeePersonnelInfo:        &personnelInfoModel4,
		EmployeeGeneralInfo:          &generalInfoModel4,
		EmployeeAddressInfo:          addressInfoModel4,
		EmployeeContactInfo:          &contactInfoModel4,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel4,
		CreatedBy:                    1,
		UpdatedBy:                    1,
		StartDate:                    helpers.StartOfDateNow(),
		EndDate:                      maxTime,
	})
	tx.Create(&model.Employee{
		UserID:                       6,
		CitizenID:                    "1409970048698",
		EmployeePersonnelInfo:        &personnelInfoModel5,
		EmployeeGeneralInfo:          &generalInfoModel5,
		EmployeeAddressInfo:          addressInfoModel5,
		EmployeeContactInfo:          &contactInfoModel5,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel5,
		CreatedBy:                    1,
		UpdatedBy:                    1,
		StartDate:                    helpers.StartOfDateNow(),
		EndDate:                      maxTime,
	})
	tx.Create(&model.Employee{
		UserID:                       7,
		CitizenID:                    "1609970048698",
		EmployeePersonnelInfo:        &personnelInfoModel6,
		EmployeeGeneralInfo:          &generalInfoModel6,
		EmployeeAddressInfo:          addressInfoModel6,
		EmployeeContactInfo:          &contactInfoModel6,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel6,
		CreatedBy:                    1,
		UpdatedBy:                    1,
		StartDate:                    helpers.StartOfDateNow(),
		EndDate:                      maxTime,
	})
	tx.Create(&model.Employee{
		UserID:                       8,
		CitizenID:                    "2099700487183",
		EmployeePersonnelInfo:        &personnelInfoModel7,
		EmployeeGeneralInfo:          &generalInfoModel7,
		EmployeeAddressInfo:          addressInfoModel7,
		EmployeeContactInfo:          &contactInfoModel7,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel7,
		CreatedBy:                    1,
		UpdatedBy:                    1,
		StartDate:                    helpers.StartOfDateNow(),
		EndDate:                      maxTime,
	})
	tx.Create(&model.Employee{
		UserID:                       9,
		CitizenID:                    "7499700347883",
		EmployeePersonnelInfo:        &personnelInfoModel8,
		EmployeeGeneralInfo:          &generalInfoModel8,
		EmployeeAddressInfo:          addressInfoModel8,
		EmployeeContactInfo:          &contactInfoModel8,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel8,
		CreatedBy:                    1,
		UpdatedBy:                    1,
		StartDate:                    helpers.StartOfDateNow(),
		EndDate:                      maxTime,
	})
	tx.Create(&model.Employee{
		UserID:                       10,
		CitizenID:                    "4119700286183",
		EmployeePersonnelInfo:        &personnelInfoModel9,
		EmployeeGeneralInfo:          &generalInfoModel9,
		EmployeeAddressInfo:          addressInfoModel9,
		EmployeeContactInfo:          &contactInfoModel9,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel9,
		CreatedBy:                    1,
		UpdatedBy:                    1,
		StartDate:                    helpers.StartOfDateNow(),
		EndDate:                      maxTime,
	})
	tx.Create(&model.Employee{
		UserID:                       11,
		CitizenID:                    "9985700286183",
		EmployeePersonnelInfo:        &personnelInfoModel10,
		EmployeeGeneralInfo:          &generalInfoModel10,
		EmployeeAddressInfo:          addressInfoModel10,
		EmployeeContactInfo:          &contactInfoModel10,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel10,
		CreatedBy:                    1,
		UpdatedBy:                    1,
		StartDate:                    helpers.StartOfDateNow(),
		EndDate:                      maxTime,
	})
	tx.Create(&model.Employee{
		UserID: 22,
		CitizenID: "9985700286223",
		EmployeePersonnelInfo: &personnelInfoModel11,
		EmployeeGeneralInfo: &generalInfoModel11,
		EmployeeAddressInfo: addressInfoModel11,
		EmployeeContactInfo: &contactInfoModel11,
		EmployeeEmergencyContactInfo: emergencyContactInfoModel11,
		CreatedBy: 1,
		UpdatedBy: 1,
		StartDate: time.Now(),
		EndDate: maxTime,
	})

	//Create User Detail
	now := time.Now()
	tx.Create(&model.UserDetail{
		UserID:          1,
		EmployeeID:      0,
		CompanyID:       0,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          2,
		EmployeeID:      1,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          3,
		EmployeeID:      2,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          4,
		EmployeeID:      3,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          5,
		EmployeeID:      4,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          6,
		EmployeeID:      5,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          7,
		EmployeeID:      6,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          8,
		EmployeeID:      7,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          9,
		EmployeeID:      8,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          10,
		EmployeeID:      9,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          11,
		EmployeeID:      10,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          12,
		EmployeeID:      0,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          13,
		EmployeeID:      0,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          14,
		EmployeeID:      0,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          15,
		EmployeeID:      0,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          16,
		EmployeeID:      0,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          17,
		EmployeeID:      0,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          18,
		EmployeeID:      0,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          19,
		EmployeeID:      0,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          20,
		EmployeeID:      0,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now.AddDate(0, 0, 90),
	})
	tx.Create(&model.UserDetail{
		UserID:          21,
		EmployeeID:      0,
		CompanyID:       3,
		DeactivatedDate: maxTime,
		ExpiredDate:     now,
	})
	tx.Create(&model.UserDetail{
		UserID: 22,
		EmployeeID: 11,
		CompanyID: 3,
		DeactivatedDate: maxTime,
		ExpiredDate: now,
	})
	return tx.Commit().Error
}
func Rollback_201908142036(tx *gorm.DB) error {
	return tx.Rollback().Error
}
