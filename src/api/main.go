package main

import (
	"api/migration"
	"api/router"
	"fmt"

	"dv.co.th/hrms/core"
	"github.com/kataras/iris"
)

func main() {
	app := core.GetApplication()

	app.Logger.Info("Main - ", *app)

	irisApp := router.New()

	irisApp.Get("/migrate", func(ctx iris.Context) {
		migration.Execute()

		ctx.JSON(iris.Map{
			"message": core.GetApplication().Env.JwtPublicKeyPath,
		})
	})
	irisApp.Get("/test_import", func(ctx iris.Context) {
		fmt.Println("1234")
	})

	// listen and serve on http://0.0.0.0:8080.
	irisApp.Run(iris.Addr(":8080"))
}
