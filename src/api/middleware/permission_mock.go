package middleware

import (
	"context"

	"dv.co.th/hrms/core/util"
)

func CheckPermission(permission string, ctx context.Context) bool {
	var isCan bool
	//TODO: Get token JWT and Call Role Permission
	if token, err := util.GetTokenFromHeader(ctx); err == nil {
		isCan = permission == token
	}
	return isCan
}
