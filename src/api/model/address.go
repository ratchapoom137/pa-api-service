package model

import "time"

//EmployeeAddressInfo table model -
type EmployeeAddressInfo struct {
	ID             int        `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	EmployeeInfoID int        `json:"employee_info_id" gorm:"column:employee_info_id;AUTO_INCREMENT"`
	AddressType    string     `json:"address_type" gorm:"column:address_type" sql:"not null" validate:"required"`
	Country        string     `json:"country" gorm:"column:country" sql:"not null" validate:"required"`
	Province       string     `json:"province" gorm:"column:province"  sql:"not null" validate:"required"`
	Address        string     `json:"address" sql:" gorm:"column:address"`
	PostalCode     string     `json:"postal_code" gorm:"column:postal_code" sql:"not null validate:"required"`
	CreatedBy      int        `json:"created_by" sql:"not null"`
	UpdatedBy      int        `json:"updated_by" sql:"not null"`
	CreatedAt      time.Time  `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt      time.Time  `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt      *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

//Assign Table name
func (EmployeeAddressInfo) TableName() string {
	return "address_info"
}