package model

import (
	"time"
)

//EmployeeGeneralInfo table model -
type EmployeeGeneralInfo struct {
	ID                int        `json:"id" gorm:"primary_key;AUTO_INCREMENT" pex:"detail:r,list:r"`
	EmployeeInfoID    int        `json:"employee_info_id" gorm:"column:employee_info_id;AUTO_INCREMENT" pex:"detail:r" `
	CompanyEmployeeID string     `json:"company_employee_id" gorm:"column:company_employee_id" sql:"not null" validate:"required" pex:"detail:r"`
	TypeOfStaff       string     `json:"type_of_staff" gorm:"column:type_of_staff" sql:"not null" validate:"required" pex:"detail:r"`
	EmployeeLevel     string     `json:"employee_level" gorm:"column:employee_level" sql:"not null" validate:"required" pex:"detail:r"`
	WorkTypeID        int        `json:"work_type_id" gorm:"column:work_type_id" sql:"not null" pex:"detail:r"`
	ProbationEndDate  *time.Time `json:"probation_end_date" gorm:"column:probation_end_date" sql:"type:datetime" pex:"detail:r"`
	PositionID        int        `json:"position_id" gorm:"column:position_id" sql:"not null" pex:"detail:r"`
	OrgUnitID         int        `json:"org_unit_id" gorm:"column:org_unit_id" sql:"not null" pex:"detail:r"`
	CompanyBranchID   int        `json:"company_branch_id" gorm:"column:company_branch_id" sql:"not null" validate:"required" pex:"detail:r"`
	CompanyID         int        `json:"company_id" gorm:"column:company_id" sql:"not null" validate:"required" pex:"detail:r"`
	CreatedBy         int        `json:"created_by" sql:"not null" pex:"detail:r"`
	UpdatedBy         int        `json:"updated_by" sql:"not null" pex:"detail:r"`
	CreatedAt         time.Time  `json:"created_at" sql:"type:datetime(6)" pex:"detail:r"`
	UpdatedAt         time.Time  `json:"updated_at" sql:"type:datetime(6)" pex:"detail:r"`
	DeletedAt         *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)" pex:"detail:r"`
}

//Assign Table name
func (EmployeeGeneralInfo) TableName() string{
	return "general_info"
}