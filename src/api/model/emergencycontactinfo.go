package model

import (
	"time"
)

//EmployeeEmergencyContactInfo table model - 
type EmployeeEmergencyContactInfo struct {
	ID                int        `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	EmployeeInfoID    int        `json:"employee_info_id" gorm:"AUTO_INCREMENT"`
	Name              string     `json:"name" gorm:"column:name"`
	Relative          string     `json:"relative" gorm:"column:relative"`
	MobilePhoneNumber string     `json:"mobile_phone_number" gorm:"column:mobile_phone_number"`
	CreatedBy         int        `json:"created_by" sql:"not null"`
	UpdatedBy         int        `json:"updated_by" sql:"not null"`
	CreatedAt         time.Time  `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt         time.Time  `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt         *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

//Assign Table name
func (EmployeeEmergencyContactInfo) TableName() string{
	return "emergncy_contact_info"
}