package model

import (
	"errors"
	"time"

	"dv.co.th/hrms/core"
	validator "gopkg.in/go-playground/validator.v9"
)

//Employee table model -
type Employee struct {
	ID                           int                            `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	UserID						 int							`json:"user_id" gorm:"column:user_id`
	CitizenID                    string                         `json:"citizen_id" gorm:"column:citizen_id;size:13;unique;not null" validate:"required,len=13,numeric"`
	EmployeePersonnelInfo        *EmployeePersonnelInfo         `json:"personnel_info,omitempty" gorm:"column:personnel_info;one2one:personnel_info;foreignkey:employee_info_id;association_foreignkey:id;"`
	EmployeeAddressInfo          []EmployeeAddressInfo          `json:"address_info,omitempty" gorm:"column:address_info;one2many:address_info;foreignkey:employee_info_id;association_foreignkey:id;"`
	EmployeeContactInfo          *EmployeeContactInfo           `json:"contact_info,omitempty" gorm:"column:contact_info;one2one:contact_info;foreignkey:employee_info_id;association_foreignkey:id;"`
	EmployeeEmergencyContactInfo []EmployeeEmergencyContactInfo `json:"emergency_contact_info,omitempty" gorm:"column:emergency_contact_info;one2one:contact_info;foreignkey:employee_info_id;association_foreignkey:id;"`
	EmployeeGeneralInfo          *EmployeeGeneralInfo           `json:"general_info,omitempty" gorm:"column:general_info;one2one:general_info;foreignkey:employee_info_id;association_foreignkey:id;"`
	CreatedBy                    int                            `json:"created_by" sql:"not null"`
	UpdatedBy                    int                            `json:"updated_by" sql:"not null"`
	CreatedAt                    time.Time                      `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt                    time.Time                      `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt                    *time.Time                     `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
	StartDate					 time.Time 						`json:"start_date" sql:"type:datetime(6)"`
	EndDate					     time.Time 						`json:"end_date" sql:"type:datetime(6)"`
}

//Assign Table name
func (Employee) TableName() string {
	return "employee"
}

//Create Validation -
func (emp *Employee) Create() (err error) {
	app := core.GetApplication()
	db := app.DB

	validate := validator.New()
	err = validate.Struct(emp)

	if err != nil {
		app.Logger.Error(err.Error())
		err = errors.New("Invalid json")
		return
	}

	if result := db.Create(&emp); result.Error != nil {
		err = result.Error
		return
	}

	return
}

//GetOutput employee information -
func (emp Employee) GetOutput(employee string) (empOject interface{}) {
	empOject = emp
	return
}
