package model
import (
	"errors"
	"time"

	"dv.co.th/hrms/core"
	validator "gopkg.in/go-playground/validator.v9"
)

type UserDetail struct {
	ID					int					`json:"id" gorm:"column:id; type:int unsigned auto_increment; primary_key; unique;"`
	UserID				int					`json:"user_id" sql:"not null" gorm:"column:user_id"`
	EmployeeID			int					`json:"employee_id" gorm:"column:employee_id"`
	CompanyID			int					`json:"company_id" gorm:"column:company_id"`
	
	// Association
	Employee 			*Employee 			`json:"employee,omitempty" gorm:"foreignkey:employee_id;association_foreignkey:id;"`

	DeactivatedDate	    time.Time			`json:"deactivated_date" sql:"type:datetime(6)"`
	ExpiredDate			time.Time			`json:"expired_date" sql:"type:datetime(6)"`
	CreatedAt           time.Time       	`json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt           time.Time       	`json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt           *time.Time      	`json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

type FilterList struct {
	CompanyIDList []int `json:"company_id_list"`
	UserIDList []int `json:"user_id_list"`
}

func (UserDetail) TableName() string {
	return "user_detail"
}

func (userDetail *UserDetail) Create() (id int, err error) {
	app := core.GetApplication()
	db := app.DB
	validate := validator.New()
	err = validate.Struct(userDetail)
	if err != nil {
		app.Logger.Error(err.Error())
		err = errors.New("Invalid json")
		return
	}

	if result := db.Create(&userDetail); result.Error != nil {
		err = result.Error
		return
	}
	id = userDetail.ID
	return
}

func (userDetail *UserDetail) Edit() (err error) {
	app := core.GetApplication()
	db := app.DB
	validate := validator.New()
	err = validate.Struct(userDetail)
	if err != nil {
		app.Logger.Error(err.Error())
		err = errors.New("invalid json")	
		return
	}
	if result := db.Model(UserDetail{ID: userDetail.ID}).Save(&userDetail); result.Error != nil {
		err = result.Error
		return
	}
	return
}