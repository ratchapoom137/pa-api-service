package model

import (
	"time"
	"errors"
	"dv.co.th/hrms/core"
	validator "gopkg.in/go-playground/validator.v9"
	// "github.com/r3labs/diff"
)

// EmployeePersonnelInfo table model -
type EmployeePersonnelInfo struct {
	ID              int        `json:"id" gorm:"primary_key;AUTO_INCREMENT" pex:"detail:r`
	EmployeeInfoID  int        `json:"employee_info_id" gorm:"column:employee_info_id;AUTO_INCREMENT" pex:"detail:r`
	TitleNameTh     string     `json:"titlename_th" gorm:"column:titlename_th" sql:"not null" validate:"required" pex:"detail:r`
	FirstNameTh     string     `json:"firstname_th" gorm:"column:firstname_th" sql:"not null" validate:"required" pex:"detail:r`
	LastNameTh      string     `json:"lastname_th" gorm:"column:lastname_th" sql:"not null" validate:"required" pex:"detail:r`
	TitleNameEn     string     `json:"titlename_en" gorm:"column:titlename_en" sql:"not null validate:"required" pex:"detail:r`
	FirstNameEn     string     `json:"firstname_en" gorm:"column:firstname_en" sql:"not null validate:"required" pex:"detail:r`
	LastNameEn      string     `json:"lastname_en" gorm:"column:lastname_en" sql:"not null validate:"required" pex:"detail:r`
	DateOfBirth     time.Time  `json:"date_of_birth" gorm:"column:date_of_birth" sql:"type:datetime;not null validate:"required" pex:"detail:r`
	Gender          string     `json:"gender" gorm:"column:gender" sql:"not null validate:"required" pex:"detail:r`
	Height          float64    `json:"height" gorm:"column:height" pex:"detail:r`
	Weight          float64    `json:"weight" gorm:"column:weight" pex:"detail:r`
	BloodType       *string    `json:"blood_type" gorm:"column:blood_type" pex:"detail:r`
	PregnancyStatus string     `json:"pregnancy_status" gorm:"column:pregnancy_status" pex:"detail:r`
	Nationality     *string    `json:"nationality" gorm:"column:nationality" pex:"detail:r`
	Religion        *string    `json:"religion" gorm:"column:religion" pex:"detail:r`
	MaritalStatus   *string    `json:"marital_status" gorm:"column:marital_status" pex:"detail:r`
	CreatedBy       int        `json:"created_by" sql:"not null" pex:"detail:r`
	UpdatedBy       int        `json:"updated_by" sql:"not null" pex:"detail:r`
	CreatedAt       time.Time  `json:"created_at" sql:"type:datetime(6)" pex:"detail:r`
	UpdatedAt       time.Time  `json:"updated_at" sql:"type:datetime(6)" pex:"detail:r`
	DeletedAt       *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)" pex:"detail:r`
}

//Assign Table name
func (EmployeePersonnelInfo) TableName() string {
	return "personnel_info"
}

func (emp *EmployeePersonnelInfo) UpdateInfo() (err error) {
	app := core.GetApplication()
	db := app.DB

	validate := validator.New()
	err = validate.Struct(emp)

	if err != nil {
		app.Logger.Error(err.Error())
		err = errors.New("Invalid json")
		return
	}

	if result := db.Updates(&emp); result.Error != nil {
		err = result.Error
		return
	}

	return
}