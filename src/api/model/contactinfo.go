package model

import "time"

//EmployeeContactInfo table model -
type EmployeeContactInfo struct {
	ID                         int                          `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	EmployeeInfoID             int                          `json:"employee_info_id" gorm:"AUTO_INCREMENT"`
	CompanyEmail               string                       `json:"company_email" gorm:"column:company_email" sql:"not null" validate:"required"`
	ContactInfoMobilePhoneInfo []ContactInfoMobilePhoneInfo `json:"contact_info_mobile_phone_info,omitempty" gorm:"column:mobile_phone_info;one2many:mobile_phone_info;foreignkey:mobile_info_id;association_foreignkey:id;"`
	CreatedBy                  int                          `json:"created_by" sql:"not null"`
	UpdatedBy                  int                          `json:"updated_by" sql:"not null"`
	CreatedAt                  time.Time                    `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt                  time.Time                    `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt                  *time.Time                   `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

//Assign Table name
func (EmployeeContactInfo) TableName() string {
	return "contact_info"
}