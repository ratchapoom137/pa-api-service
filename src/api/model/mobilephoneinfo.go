package model

import "time"

//ContactInfoMobilePhoneInfo table model -
type ContactInfoMobilePhoneInfo struct {
	ID                int        `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	MobileInfoID      int        `json:"mobile_info_id" gorm:"AUTO_INCREMENT"`
	MobilePhoneNumber string     `json:"mobile_phone_number" gorm:"column:mobile_phone_number"`
	CreatedBy         int        `json:"created_by" sql:"not null"`
	UpdatedBy         int        `json:"updated_by" sql:"not null"`
	CreatedAt         time.Time  `json:"created_at" sql:"type:datetime(6)"`
	UpdatedAt         time.Time  `json:"updated_at" sql:"type:datetime(6)"`
	DeletedAt         *time.Time `json:"deleted_at,omitempty" sql:"type:datetime(6)"`
}

//Assign Table name
func (ContactInfoMobilePhoneInfo) TableName() string{
	return "contact_info_mobile_phone_info"
}