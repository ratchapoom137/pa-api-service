package constant

// ReturnMap is a return schema for api rest
type ReturnMap struct {
	Code    string
	Message string
}

//ReturnConst -
var ReturnConst = map[string]ReturnMap{
	"Success": ReturnMap{
		Code:    "SUCCESS",
		Message: "Success",
	},

	//ReturnEmployeeConstSuccess -
	"CreateEmployeeSuccessConst": ReturnMap{
		Code:    "EMPLOYEE_CREATE_SUCCESS",
		Message: "Employee create success",
	},
	"SearchEmployeeSuccessConst": ReturnMap{
		Code:    "EMPLOYEE_SEARCH_SUCCESS",
		Message: "Employee search success",
	},
	"UpdateEmployeeSuccessConst": ReturnMap{
		Code:    "EMPLOYEE_UPDATE_SUCCESS",
		Message: "Employee update success",
	},

	//ReturnUserDetailConstSuccess -
	"CreateUserDetailSuccessConst": ReturnMap{
		Code:    "USER_DETAIL_CREATE_SUCCESS",
		Message: "User detail create success",
	},
	"SearchUserDetailSuccessConst": ReturnMap{
		Code:    "USER_DETAIL_SEARCH_SUCCESS",
		Message: "User detail search success",
	},
	"DeleteUserDetailSuccessConst": ReturnMap{
		Code:    "DELETE_USER_DETAIL_SUCCESS",
		Message: "Delete User detail success",
	},
	"UpdateUserDetailSuccessConst": ReturnMap{
		Code:    "UPDATE_USER_DETAIL_SUCCESS",
		Message: "Update User detail success",
	},


	//ReturnEmployeeConstFailed -
	"ReadEmployeeJsonFailConst": ReturnMap{
		Code:    "READ_EMPLOYEE_JSON_FAIL",
		Message: "Read employee json fail",
	},
	"CreateEmployeeFailedConst": ReturnMap{
		Code:    "EMPLOYEE_CREATE_FAILED",
		Message: "Employee create failed",
	},
	"EmployeeNotFoundConst": ReturnMap{
		Code:    "EMPLOYEE_NOT_FOUND",
		Message: "Employee not found",
	},
	"UpdateEmployeeFailedConst": ReturnMap{
		Code:    "EMPLOYEE_UPDATE_FAIL",
		Message: "Employee update failed",
	},

	//ReturnUsetDetailConstFailed -
	"ReadUserDetailJsonFailedConst": ReturnMap{
		Code:    "READ_USER_DETAIL_JSON_FAIL",
		Message: "Read user detail json fail",
	},
	"CreateUserDetailFailedConst": ReturnMap{
		Code:    "USER_DETAIL_CREATE_FAILED",
		Message: "User detail create failed",
	},
	"UserDetailNotFoundConst": ReturnMap{
		Code:    "USER_DETAIL_NOT_FOUND",
		Message: "User detail not found",
	},
	"DeleteUserDetailFailedConst": ReturnMap{
		Code:    "DELETE_USER_DETAIL_FAILED",
		Message: "Delete user detail failed",
	},
	"UpdateUserDetailFailedConst": ReturnMap{
		Code:    "UPDATE_USER_DETAIL_FAILED",
		Message: "Update user detail failed",
	},


}

