package rest

import (
	"api/constant"
	"api/model"
	"api/service"
	"encoding/json"
	"strconv"

	"dv.co.th/hrms/core/rest"
	"github.com/kataras/iris"
)

// PostUserDetail godoc
// @Summary Add a user detail for user account
// @Description
// @Router /pa/user-detail [post]
func PostUserDetail(ctx iris.Context) {
	var userDetail model.UserDetail
	err := ctx.ReadJSON(&userDetail)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["ReadUserDetailJsonFailedConst"].Message,
				Code:    constant.ReturnConst["ReadUserDetailJsonFailedConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	outputResult, err := service.CreateUserDetail(userDetail)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["CreateUserDetailFailedConst"].Message,
				Code:    constant.ReturnConst["CreateUserDetailFailedConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	successResponse := rest.APIResult{
		Data: outputResult,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["CreateUserDetailSuccessConst"].Message,
			Code:    constant.ReturnConst["CreateUserDetailSuccessConst"].Code,
		},
	}
	rest.Success(ctx, successResponse)
}

// GetUserDetail godoc
// @Summary Get user detail by id
// @Description
// @Router /pa/user-detail/{id} [get]
func GetUserDetail(ctx iris.Context) {
	UserDetailID, _ := strconv.Atoi(ctx.Params().Get("id"))

	outputResult, err := service.GetUserDetailByID(UserDetailID)

	var userDetail service.UserDetailOutPut
	jsonData, _ := json.Marshal(outputResult)
	json.Unmarshal(jsonData, &userDetail)

	userDetail.FirstName = outputResult.Employee.EmployeePersonnelInfo.FirstNameEn
	userDetail.LastName = outputResult.Employee.EmployeePersonnelInfo.LastNameEn

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["UserDetailNotFoundConst"].Message,
				Code:    constant.ReturnConst["UserDetailNotFoundConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	successResponse := rest.APIResult{
		Data: userDetail,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["SearchUserDetailSuccessConst"].Message,
			Code:    constant.ReturnConst["SearchUserDetailSuccessConst"].Code,
		},
	}
	rest.Success(ctx, successResponse)
}

// GetUserDetails godoc
// @Summary Get user details
// @Description
// @Router /pa/user-details [get]
func GetUserDetails(ctx iris.Context) {
	var FilterList model.FilterList
	err := ctx.ReadJSON(&FilterList)

	filter := map[string]interface{}{
		"user_id":         ctx.URLParam("user_id"),
		"user_id_list":    FilterList.UserIDList,
		"employee_id":     ctx.URLParam("employee_id"),
		"company_id_list": FilterList.CompanyIDList,
		"name":            ctx.URLParam("name"),
		"status":          ctx.URLParam("status"),
		"limit":           ctx.URLParamDefault("limit", "9999"),
		"offset":          ctx.URLParamDefault("offset", "0"),
	}

	outputResult, total, err := service.GetUserDetailList(filter)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["UserDetailNotFoundConst"].Message,
				Code:    constant.ReturnConst["UserDetailNotFoundConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 404)
		return
	}

	successResponse := rest.APIResult{
		Data:  outputResult,
		Total: total,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["SearchUserDetailSuccessConst"].Message,
			Code:    constant.ReturnConst["SearchUserDetailSuccessConst"].Code,
		},
	}
	rest.SuccessSearch(ctx, successResponse)
}

// Delete godoc
// @Summary Delete user detail
// @Description
// @Router /pa/user-detail [delete]
func DeleteUserDetail(ctx iris.Context) {
	userDetailID, _ := strconv.Atoi(ctx.Params().Get("id"))

	err := service.DeleteUserDetail(userDetailID)

	if err != nil {
		err := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["DeleteUserDetailFailedConst"].Message,
				Code:    constant.ReturnConst["DeleteUserDetailFailedConst"].Code,
			},
		}
		rest.Fail(ctx, err, 404)
		return
	}

	successResponse := rest.APIResult{
		Data: userDetailID,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["DeleteUserDetailSuccessConst"].Message,
			Code:    constant.ReturnConst["DeleteUserDetailSuccessConst"].Code,
		},
	}
	rest.SuccessSearch(ctx, successResponse)
}

// Put godoc
// @Summary Update user detail
// @Description
// @Router /pa/user-detail [put]
func PutUserDetail(ctx iris.Context) {
	var userDetail model.UserDetail
	err := ctx.ReadJSON(&userDetail)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["ReadUserDetailJsonFailedConst"].Message,
				Code:    constant.ReturnConst["ReadUserDetailJsonFailedConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	userDetailID, _ := strconv.Atoi(ctx.Params().Get("id"))
	outputError := service.EditUserDetail(userDetailID, userDetail)

	if outputError != nil {
		err := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["UpdateUserDetailFailedConst"].Message,
				Code:    constant.ReturnConst["UpdateUserDetailFailedConst"].Code,
			},
		}
		rest.Fail(ctx, err, 404)
		return
	}

	successResponse := rest.APIResult{
		Data: userDetailID,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["UpdateUserDetailSuccessConst"].Message,
			Code:    constant.ReturnConst["UpdateUserDetailSuccessConst"].Code,
		},
	}
	rest.SuccessSearch(ctx, successResponse)

}
