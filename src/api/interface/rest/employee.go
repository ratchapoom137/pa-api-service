package rest

import (
	"api/base"
	"api/constant"
	"api/model"
	"api/service"
	"strconv"

	// "github.com/davecgh/go-spew/spew"
	"dv.co.th/hrms/core/rest"
	"github.com/kataras/iris"
)

// PostEmployee godoc
// @Summary Add a new employee
// @Description
// @Tags employee:employee
// @Param user body request.APICreateEmployee true "Add employee"
// @Router /employee/employee [post]
func PostEmployee(ctx iris.Context) {
	var employee model.Employee
	err := ctx.ReadJSON(&employee)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["ReadEmployeeJsonFailConst"].Message,
				Code:    constant.ReturnConst["ReadEmployeeJsonFailConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	emp, err := service.CreateEmployee(employee)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["CreateEmployeeFailedConst"].Message,
				Code:    constant.ReturnConst["CreateEmployeeFailedConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	successResponse := rest.APIResult{
		Data: emp,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["CreateEmployeeSuccessConst"].Message,
			Code:    constant.ReturnConst["CreateEmployeeSuccessConst"].Code,
		},
	}
	rest.Success(ctx, successResponse)
}

// GetEmployee godoc
// @Summary GET employee and all information by id
// @Description
// @Tags employee:employee
// @Param user body request.APIGetEmployeeById true "get employee"
// @Router /pa/employee/employee/{id} [get]
func GetEmployee(ctx iris.Context) {
	app := base.GetApplication()

	ID, _ := strconv.Atoi(ctx.Params().Get("personal_id"))

	app.Logger.Infof("Get employee with %d", ID)
	Employee, ok, result, total := service.GetEmployeeFromId(ID)

	if ok {
		resultJSON := rest.APIResult{
			Data: Employee,
			Total: total,
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["SearchEmployeeSuccessConst"].Message,
				Code:    constant.ReturnConst["SearchEmployeeSuccessConst"].Code,
			},
		}
		rest.SuccessSearch(ctx, resultJSON)
		return
	}

	rest.Error(ctx, rest.APIResult{
		Status: rest.APIResultStatus{
			Code:    result.Code,
			Message: result.Message,
		},
	})
	return
}

// GetEmployeeFromIdWithInfoByInputInfoType godoc
// @Summary GET employee information by id and infotype
// @Description
// @Tags employee:employeeWithInfoByInfoType
// @Param user body request.APIGetWithGeneralInfo true "get employeeWithGeneralInfo"
// @Router /pa/employee/employeeWithGeneralInfo/{id} [get]
func GetEmployeeFromIdWithInfoByInputInfoType(ctx iris.Context) {
	app := base.GetApplication()

	ID, _ := strconv.Atoi(ctx.Params().Get("personal_id"))
	InfoTypeName := ctx.Params().Get("infotype_name")

	app.Logger.Infof("get employee with %d", ID)
	EmployeeWithInfo, ok, result := service.GetEmployeeFromIdWithInfoByInputInfoType(ID, InfoTypeName)

	if ok {
		resultJSON := rest.APIResult{
			Data: EmployeeWithInfo,
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["FoundEmployeeSuccessConst"].Message,
				Code:    constant.ReturnConst["FoundEmployeeSuccessConst"].Code,
			},
		}
		rest.Success(ctx, resultJSON)
		return
	}

	rest.Error(ctx, rest.APIResult{
		Status: rest.APIResultStatus{
			Code:    result.Code,
			Message: result.Message,
		},
	})
	return
}

// GetEmployees godoc
// @Summary GET all employee and all information
// @Description
// @Tags employee:employees
// @Param user body request.APIGetEmployees true "get employees"
// @Router /pa/employee/employees [get]
func GetEmployees(ctx iris.Context) {
	name := ctx.FormValue("name")
	status := ctx.FormValue("status")
	employeeID := ctx.FormValue("company_employee_id")
	companyID := ctx.FormValue("company_id")
	userID := ctx.FormValue("user_id")
	gender := ctx.FormValue("gender")
	positionID := ctx.FormValue("position_id")
	pointOfTime := ctx.FormValue("point_of_time")
	citizenID := ctx.FormValue("citizen_id")
	companyEmail := ctx.FormValue("company_email")
	workTypeID := ctx.FormValue("work_type_id")
	limit := ctx.FormValue("limit")
	offset := ctx.FormValue("offset")

	filter := map[string]interface{}{}

	if name != "" {
		filter["name"] = name
	}
	if status != "" {
		filter["status"] = status
	}
	if employeeID != "" {
		filter["company_employee_id"] = employeeID
	}
	if companyID != "" {
		filter["company_id"] = companyID
	}
	if userID != "" {
		filter["user_id"] = userID
	}
	if gender != "" {
		filter["gender"] = gender
	}
	if positionID != "" {
		filter["position_id"] = positionID
	}
	if pointOfTime != "" {
		filter["point_of_time"] = pointOfTime
	}
	if citizenID != "" {
		filter["citizen_id"] = citizenID
	}
	if companyEmail != "" {
		filter["company_email"] = companyEmail
	}
	if workTypeID != "" {
		filter["work_type_id"] = workTypeID
	}
	if limit == "" && offset == "" {
		limit = "-1"
		offset = "-1"
	}
	filter["limit"] = limit
	filter["offset"] = offset

	Employees, ok, result, total := service.GetEmployeeLists(filter)

	if ok {
		resultJSON := rest.APIResult{
			Data:  Employees,
			Total: total,
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["SearchEmployeeSuccessConst"].Message,
				Code:    constant.ReturnConst["SearchEmployeeSuccessConst"].Code,
			},
		}
		rest.SuccessSearch(ctx, resultJSON)
		return
	}

	rest.Error(ctx, rest.APIResult{
		Status: rest.APIResultStatus{
			Code:    result.Code,
			Message: result.Message,
		},
	})
	return
}

// PutEmployee godoc
// @Summary update employee information
// @Description
// @Tags employee:employeeInfo
// @Param user body request.APIPutEmployee true "Update employee information"
// @Router /employee/employeeInfo [put]
func PutEmployee(ctx iris.Context) {
	var employee interface{}

	app := base.GetApplication()
	ID, _ := strconv.Atoi(ctx.Params().Get("personal_id"))
	app.Logger.Infof("get employee id with %d", ID)
	IntoTypeName := ctx.Params().Get("infotype_name")
	app.Logger.Infof("get information type name with %d", IntoTypeName)

	err := ctx.ReadJSON(&employee)

	if ID <= 0 {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["EmployeeIDInvalid"].Message,
				Code:    constant.ReturnConst["EmployeeIDInvalid"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	if err != nil {

		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["ReadEmployeeJsonFailConst"].Message,
				Code:    constant.ReturnConst["ReadEmployeeJsonFailConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	employeeInformation := map[string]interface{}{}

	switch IntoTypeName {
	case "personnel_info":
		employeeInformation["personnel_info"] = map[string]interface{}{
			"employee_id": ID,
			"data":        employee,
		}
	case "general_info":
		employeeInformation["general_info"] = map[string]interface{}{
			"employee_id": ID,
			"data":        employee,
		}
	case "address_info":
		employeeInformation["address_info"] = map[string]interface{}{
			"employee_id": ID,
			"data":        employee,
		}
	case "contact_info":
		employeeInformation["contact_info"] = map[string]interface{}{
			"employee_id": ID,
			"data":        employee,
		}
	case "emergncy_contact_info":
		employeeInformation["emergncy_contact_info"] = map[string]interface{}{
			"employee_id": ID,
			"data":        employee,
		}
	default:
		return
	}

	result, err := service.UpdateEmployee(employeeInformation)

	if err != nil {
		errResponse := rest.APIResult{
			Status: rest.APIResultStatus{
				Message: constant.ReturnConst["UpdateEmployeeFailedConst"].Message,
				Code:    constant.ReturnConst["UpdateEmployeeFailedConst"].Code,
			},
		}
		rest.Fail(ctx, errResponse, 400)
		return
	}

	successResponse := rest.APIResult{
		Data: result,
		Status: rest.APIResultStatus{
			Message: constant.ReturnConst["UpdateEmployeeSuccessConst"].Message,
			Code:    constant.ReturnConst["UpdateEmployeeSuccessConst"].Code,
		},
	}
	rest.Success(ctx, successResponse)
}
