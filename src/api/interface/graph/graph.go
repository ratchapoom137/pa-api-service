package graph

import "github.com/graphql-go/graphql"

// Controller -
type Controller struct{}
type ControllerReturn func() (string, *graphql.Field)

// GetQueryMapping -
func (c *Controller) GetQueryMapping() []ControllerReturn {
	return []ControllerReturn{}
}

// GetMutationMapping -
func (c *Controller) GetMutationMapping() []ControllerReturn {
	return []ControllerReturn{}
}
