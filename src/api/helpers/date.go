package helpers

import (
	"fmt"
	"time"
)

func StartOfDateNow() time.Time {
	bangkok, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		fmt.Println("Something wrong with Load Location")
	}
	now := time.Now().In(bangkok)
	year, month, day := now.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, now.Location())
}
