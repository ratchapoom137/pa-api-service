package router

import (
	"api/interface/graph"

	"dv.co.th/hrms/core/middleware"
	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"

	"github.com/kataras/iris"
	"github.com/thoas/go-funk"
)

var controller = new(graph.Controller)

// GraphQLRouter -
func GraphQLRouter(irisApp *iris.Application) *iris.Application {

	// Schema
	queryFields := graphql.Fields{
		"hello": &graphql.Field{
			Type: graphql.String,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				return "world", nil
			},
		},
	}

	mutationFields := graphql.Fields{}

	queryMapping := controller.GetQueryMapping()
	mutationMapping := controller.GetMutationMapping()

	funk.ForEach(queryMapping, func(cr graph.ControllerReturn) {
		name, field := cr()
		queryFields[name] = field
	})

	funk.ForEach(mutationMapping, func(cr graph.ControllerReturn) {
		name, field := cr()
		mutationFields[name] = field
	})

	rootQuery := graphql.ObjectConfig{Name: "RootQuery", Fields: queryFields}
	rootMutation := graphql.ObjectConfig{Name: "RootMutation", Fields: mutationFields}

	schemaConfig := graphql.SchemaConfig{
		Query:    graphql.NewObject(rootQuery),
		Mutation: graphql.NewObject(rootMutation)}

	schema, _ := graphql.NewSchema(schemaConfig)

	h := handler.New(&handler.Config{
		Schema:     &schema,
		Pretty:     true,
		GraphiQL:   true,
		Playground: true,
	})

	cors := middleware.Cors(irisApp)

	cors.Get("/graphql", iris.FromStd(middleware.HTTPHeader(h)))
	cors.Post("/graphql", iris.FromStd(middleware.HTTPHeader(h)))

	return irisApp
}
