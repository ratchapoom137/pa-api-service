package router

import (
	"api/base"
	"api/interface/rest"

	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
)

// RestRouter -
func RestRouter(irisApp *iris.Application) *iris.Application {
	app := base.GetApplication()
	crs := cors.New(cors.Options{
		AllowedMethods:   []string{"HEAD", "GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})

	cors := irisApp.Party(app.Env.UriBasePath+"/", crs).AllowMethods(iris.MethodOptions)

	{
		cors.StaticWeb("/docs", "./docs")

		// Employee API
		cors.Handle("POST", "/employee", rest.PostEmployee)
		cors.Handle("GET", "/employee/{personal_id:int}", rest.GetEmployee)
		cors.Handle("GET", "/employees", rest.GetEmployees)
		cors.Handle("GET", "/employee/{infotype_name:string}/{personal_id:int}/", rest.GetEmployeeFromIdWithInfoByInputInfoType)
		cors.Handle("PUT", "/employee/{infotype_name:string}/{personal_id:int}", rest.PutEmployee)

		// User Detail API
		cors.Handle("POST", "/user-detail", rest.PostUserDetail)
		cors.Handle("GET", "/user-detail/{id:int}", rest.GetUserDetail)
		cors.Handle("GET", "/user-details", rest.GetUserDetails)
		cors.Handle("DELETE", "/user-detail/{id:int}", rest.DeleteUserDetail)
		cors.Handle("PUT", "/user-detail/{id:int}", rest.PutUserDetail)
	}

	return irisApp
}
