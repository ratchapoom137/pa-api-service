package router

import (
	"api/base"

	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/recover"
)

func New() *iris.Application {
	app := base.GetApplication()
	route := iris.New()

	route.Handle("GET", app.Env.UriBasePath, func(ctx iris.Context) {
		ctx.HTML("hrms api")
	})

	route = RestRouter(route)
	route = GraphQLRouter(route)
	route.Use(recover.New())

	return route
}
